clear; close all; clc;

%fs = 80e6/28/28;
fs = 500e3/14;
ch = 256;
src_freq = 400e3;
fts = 455e3;
hetero_freq = 200e3;
bandwidth = 15e3;
baseband_freq = 600e3;
adc_sampling_freq = 2.5e6;

%pwd = 3000e-6;
pwd = 350e-6;
%snr = 10;

bottom_pts = 500;

beam_count = 512;
watertank_scan_angle = linspace(-70, 70, beam_count);
simulation = false;

%% generate/load raw data 

[ m_subarray ] = subarray_sim(...
    ch, adc_sampling_freq, fs, hetero_freq, baseband_freq, bandwidth, src_freq, fts, pwd, bottom_pts, 'cw', simulation, watertank_scan_angle);
%[ multiple_idx ] = m_subarray.GetSubarrayElementIndex('Multiple');
%[ m_subarray ] = m_subarray.SetNoiseLevel(snr);

%% beamform

[ b_raw ] = m_subarray.Beamforming();
%[ two_split_idx ] = m_subarray.GetSubarrayElementIndex('Conventional');
%[ b_split ] = m_subarray.GetBeamformResult(b_raw, two_split_idx);
[ multiple_idx ] = m_subarray.GetSubarrayElementIndex('Multiple');
[ b_multi ] = m_subarray.GetBeamformResult(b_raw, multiple_idx);

b_full = abs(mean(b_raw, 3));
b_full = b_full ./ max(b_full, [], 1);

figure; imagesc(b_full);
[ cost ] = m_subarray.LeastSquare(multiple_idx, b_multi, multiple_idx, b_full);
figure; imagesc(cost); title('cost value'); colorbar(); caxis([0 50]);
cost_med = medfilt2(cost, [3,3]);
figure; imagesc(cost_med); title('cost median'); colorbar(); caxis([0 50]);

aa = cost_med./max(cost_med, [],2);
b_full = b_full ./ max(b_full, [], 2);
figure;
for idx = 1:size(aa,1)
    %plot(smoothdata(aa(:,idx)));
    plot(smoothdata(aa(idx, :)));
    
    hold on;
    plot(smoothdata(b_full(idx, :)))
    hold off;
    title(num2str(idx));

    pause(0.35);


end

