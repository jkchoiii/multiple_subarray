function [data, data_info, FLS_colormap] = get_mbes_data_info()


[file, path] = uigetfile('./MBES 데이터/20191030/CW/*.TEXT','Select a File');

if isequal(file, 0)
    assert(0~=0, "파일선택안함");
end

temp = split(path(1:end-1), '\');
sel_pwd = str2double(temp{end});


temp = split(file(1:end-5), ' ');

valid_idx = [];
for idx = 1:length(temp)
    if ~isempty(temp{idx})
        valid_idx = [valid_idx; idx];
    end
end
temp = temp(valid_idx);
for idx = 1:length(temp)
    splited_temp = split(temp{idx}, '-');
    token = splited_temp{1};
    val = str2double(splited_temp{2});
    if strcmp('Range', token)
        range = val; %meter
        
    %elseif strcmp('PWD', token)
        
    elseif strcmp('TVG', token)
        tvg = val;

    elseif strcmp('Gain', token)
        gain = val;
        
    elseif strcmp('Data', token)
        data_count = val;
        
    end

end

data_info = [];
data_info.range = range;
data_info.pwd = sel_pwd;
data_info.tvg = tvg;
data_info.gain = gain;
data_info.data_count = data_count;

csv_list = dir(fullfile(path, '*.csv'));
for idx = 1:length(csv_list)
    sel_csv = csv_list(idx);
    temp = split(sel_csv.name, '_');
    data_path = fullfile(sel_csv.folder, sel_csv.name);
    if strcmp(temp{2}(1:3), 'Mag')
        mag = load(data_path);
        
    else
        phase = load(data_path);
    end
    
end
data = [];
data.real = (mag - 8192) ./ 8192;
data.imag = (phase - 8192) ./ 8192;

load('FLS_colormap')
FLS_colormap = FLS_colormap/255;

end

