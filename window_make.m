function [weight,Compensation_value] = window_make(ch,win_select)

switch win_select
    case 1
        disp('blackmanharris window 적용 중..')
        weight = blackmanharris(ch);
        Compensation_value = 1 / 0.3475;
    case 2
        disp('chebwin window 적용 중..')
        weight = chebwin(ch);
        Compensation_value = 1 / 0.3699;
    case 3
        disp('kaiser window 적용 중..')
%         beta = 0.5842-(ch-21)^0.4 + 0.07886*(ch-21);
        weight = kaiser(ch,0.95);
        Compensation_value = 1 / 0.3789;
    case 4
        disp('hamming window 적용 중..')
        weight = hamming(ch);
        Compensation_value = 1 / 0.5256;
    case 5
        disp('hann window 적용 중..')
        weight = hann(ch);
        Compensation_value = 1 / 0.4844;
    case 6
        disp('tukeywin window 적용 중..')
        weight = tukeywin(ch,0.8);
        Compensation_value = 1 / 0.5813;
    case 7 
        disp('Chebyshev window 적용 중..')
        weight = cheby2(ch,40, 0.8,'low')';
        weight = weight(1:end-1);
        Compensation_value = 1 / 0.5813;
    case 8
        weight = ones(ch,1);
        Compensation_value = 1;
end
