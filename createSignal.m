function superposition_signal = createSignal(xPos, yPos, zPos, f, c, theta_arrival_angles, ref_signal)

superposition_signal = 0;
for k = 1:numel(theta_arrival_angles)

    doa = squeeze(steeringVector(xPos, yPos, zPos, f, c, theta_arrival_angles(k)));

        signal = doa*ref_signal;
    
    superposition_signal = superposition_signal + signal;
      
end
superposition_signal = superposition_signal';