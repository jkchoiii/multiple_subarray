classdef subarray_sim
    %SUBARRAY_SIM 이 클래스의 요약 설명 위치
    %   자세한 설명 위치
    
    properties
        num_sensors
        fs_adc
        fs
        fh
        fbase
        src_freq
        fts
        bandwidth
        sensor_pos
        pwd
        sim_info
        pulse_type
        IQ
        simulation
        
    end
    
    methods
        function [ obj ] = subarray_sim(num_sensors, fs_adc, fs, fh, fbase, bw, src_freq, fts, pwd, bottom_pts, pulse_type, simulation, watertank_scan_angle)
            obj.num_sensors = num_sensors;
            obj.fs_adc = fs_adc;
            obj.fs = fs;
            obj.fh = fh;
            obj.fts = fts;
            obj.bandwidth = bw;
            obj.pwd = pwd;
            obj.src_freq = src_freq;
            obj.fbase = fbase;
            obj.simulation = simulation;
            if obj.simulation == true
                
                if strcmpi(pulse_type, 'cw')
                    obj.pulse_type = 'cw';

                elseif strcmpi(pulse_type, 'lfm')
                    obj.pulse_type = 'lfm';

                else
                    assert(0~=0, 'pulse type error');
                end
                
                %[ obj.sensor_pos ] = GenerateSensorGeometry(obj, num_sensors, fh);
                [ obj.sensor_pos ] = GenerateSensorGeometry(obj, num_sensors, fts);
                [ obj.sim_info ] = GenerateBottomGeometry(obj, bottom_pts);
                [ obj.IQ ] = GenerateSimulationIQData(obj);
            
            else
                [ obj.sensor_pos, baseline ] = GenerateSensorGeometry(obj, num_sensors, fts);
                [ load_data, data_info, colormap] = GetExperimentsData(obj);
                %data_info.pulse_type = 'lfm';
                [ obj ] = GetMatchedFilterData(obj, load_data, data_info, watertank_scan_angle);
                
                c = 1480;
                lambda_s = c/obj.fs;
                lambda_t = c/obj.src_freq;
                beam_count = length(watertank_scan_angle);
                [obj.sim_info.SP_final, obj.sim_info.HS_final] = ...
                    delay_cal(baseline,watertank_scan_angle,lambda_t,lambda_s,obj.num_sensors,beam_count,c,1);
                
                
            end
            
        end
        
        function [ obj ] = GetMatchedFilterData(obj, load_data, data_info, watertank_scan_angle)
            
            if strcmpi(data_info.pulse_type, 'cw')
                pwd =load('CW_PWD.mat');
                obj.pwd = pwd.CW_PWD(data_info.pwd);
                %raw_t = 0:(1/Final_fs):pwd;
                sample_p = round(obj.pwd/(1/obj.fs));
                [matched_weight, comp] = window_make(sample_p, 4); %hamming
                
                zeros_data = zeros(36,1);
                ones_data = ones(sample_p,1);
                zeros_data((36/2)-(sample_p/2)+1:(36/2)+(sample_p/2)) = ones_data.*matched_weight;
                ref_sig_real=zeros_data./sample_p;
                ref_sig_imag=zeros_data./sample_p;
                
                obj.pulse_type = 'cw';
            else
                pwd = load('CP_PWD.mat');
                obj.pwd = pwd.CP_PWD(data_info.pwd);
                
                start_freq = obj.src_freq-obj.bandwidth/2;
                %end_freq = freq_center+obj.bandwidth/2;

                raw_t = 0:(1/obj.fs_adc):obj.pwd;
                chirp = obj.bandwidth/obj.pwd;
                ref_lfm = sin(2*pi*(chirp/2*raw_t+start_freq).*raw_t)';
                [matched_weight, comp] = window_make(length(raw_t), 4); %hamming
                % % 600k bandpass
                ref_lfm = ref_lfm.*sin(2*pi*(obj.fh)*raw_t)';
                filter_hetero = load('bpf590_610_flat_top.mat');
                ref_sig=filter(filter_hetero.Num, 1, (ref_lfm)).*matched_weight;
                %     pspectrum(ref_sig,raw_t)

                % IQ modulation real image 생성 600k에서 600k를 곱해 0KHz로 옮김..신호 생성W

                ref_sig_real = ref_sig.*cos(2*pi*obj.fbase*raw_t)';
                ref_sig_imag = ref_sig.*(-sin(2*pi*obj.fbase*raw_t))';

                % IQ modulation real image 생성 30khz 로우페스필터
                filter_lowpass = load('lpf30_chev.mat'); %cp 일때
                ref_sig_real = filter(filter_lowpass.Num,1,ref_sig_real);
                ref_sig_imag = filter(filter_lowpass.Num,1,ref_sig_imag);

                % 500/14로 다운샘플
                down_cof = round(obj.fs_adc/obj.fs);
                down_t = downsample(raw_t,down_cof);
                ref_sig_real =  downsample(ref_sig_real,down_cof);
                ref_sig_imag =  downsample(ref_sig_imag,down_cof);

                %sample_p = length(down_t);
                obj.pulse_type = 'lfm';
            end
            
            obj.sim_info.amplitude = comp;
            obj.sim_info.bot_angle = watertank_scan_angle;
            
            ref_sig_real = ref_sig_real ./ max(ref_sig_real);
            ref_sig_real = ref_sig_real ./ length(ref_sig_real);
            
            ref_sig_imag = ref_sig_imag ./ max(ref_sig_imag);
            ref_sig_imag = ref_sig_imag ./ length(ref_sig_imag);
            
            ref_sig = [];
            ref_sig.real = ref_sig_real;
            ref_sig.imag = ref_sig_imag;
            
            %IQ = load_data.real -1j*load_data.imag;
            AC = zeros(obj.num_sensors, size(load_data.real, 2));
            BD = AC;
            BC = AC;
            AD = AC;

            for i = 1:obj.num_sensors
                AC(i,:) = filter((ref_sig_real),1,load_data.real(i,:));
                BD(i,:) = filter((ref_sig_imag),1,load_data.imag(i,:));
                BC(i,:) = filter((ref_sig_real),1,load_data.imag(i,:));
                AD(i,:) = filter((ref_sig_imag),1,load_data.real(i,:));
            end

            xcorr_data_real = AC+BD;
            xcorr_data_imag = BC-AD;
            %xcorr_data_real = load_data.real;
            %xcorr_data_imag = load_data.imag;
            
            obj.IQ = xcorr_data_real-1i*xcorr_data_imag;
            obj.IQ = obj.IQ.';
            
        end
        
        function [ obj ] = SetNoiseLevel(obj, snr)
            obj.IQ = awgn(obj.IQ, snr, 'measured');
            
        end
        
        function [ IQ ] = GenerateSimulationIQData(obj)
            
            wt = AngularFreqVector(obj);
            kx = SteeringVector(obj, obj.sensor_pos, obj.src_freq, obj.sim_info.bot_angle);
            
            IQ_part = complex(zeros(1, obj.sim_info.max_sample));
            IQ_part(1:length(wt)) = wt.*hamming(length(wt)).';
            IQ_part = repmat(IQ_part, [obj.num_sensors, 1]).';
            
            IQ = complex(zeros(obj.sim_info.max_sample, obj.num_sensors));
            
            % time delay
            bottom_pts = numel(obj.sim_info.bot_dist);
            
            for idx = 1:bottom_pts
                delayed = delayseq(IQ_part, obj.sim_info.time_delay(idx), obj.fs) .* kx(idx,:);
                IQ = IQ + delayed.*obj.sim_info.amplitude(idx);
                disp_msg = ['Make signal.. ' , num2str((idx/bottom_pts)*100) , '%'];
                disp(disp_msg)
            end
            
        end
        
        function [exp_wt] = AngularFreqVector(obj)
            t = 0:1/obj.fs:obj.pwd;
            
            % generate basebanding signal
            if strcmpi(obj.pulse_type, 'cw')
                exp_wt = exp(1j*2*pi*0*t);
                
            else %lfm
                start_freq = -obj.bandwidth/2;
                %end_freq = obj.bandwidth/2;
                chirp = obj.bandwidth / obj.pwd;
                
                exp_wt = exp(1j*2*pi*(chirp/2*t+start_freq).*t);
                
            end
            
        end
        
        function [exp_kx] = SteeringVector(obj, pos, freq, ang)
            
            c = 1480;
            
            % angle to radian
            phi_angles = 0;
            theta_angles = ang*pi/180;
            phi_angles = phi_angles*pi/180;
            
            k = 2*pi*freq/c;
            
            
            % spherical coord.
            N = numel(phi_angles);
            
            x = sin(theta_angles)'*cos(phi_angles);
            y = sin(theta_angles)'*sin(phi_angles);
            z = repmat(cos(theta_angles)', 1, N);
            
            % steering vector
            xx = bsxfun(@times, x, reshape(pos.x, 1, 1, obj.num_sensors));
            yy = bsxfun(@times, y, reshape(pos.y, 1, 1, obj.num_sensors));
            zz = bsxfun(@times, z, reshape(pos.z, 1, 1, obj.num_sensors));
            
            exp_kx = exp(1j*k*(xx+yy+zz));
            exp_kx = squeeze(exp_kx);
            
        end
        
        function [ sim_info ] = GenerateBottomGeometry(obj, bottom_pts)
            c = 1480;
            
            x = linspace(-50, 50, bottom_pts);
            y = ones(1, bottom_pts)*5;
            
            sim_info = [];
            
            sim_info.bot_dist = vecnorm([x; y]);
            sim_info.bot_angle = atan2(x, y)*180/pi;
            sim_info.time_delay = sim_info.bot_dist/c;
            sim_info.sample_delay = sim_info.time_delay/(1/obj.fs);
            sim_info.max_sample = round(max(sim_info.sample_delay)*1.5);
            sim_info.amplitude = 1./(10.^(sim_info.bot_dist./20));
            
            sim_info.amplitude = sim_info.amplitude./max(sim_info.amplitude);
            
        end
        
        function [ pos, baseline ] = GenerateSensorGeometry(obj, num_sensors, src_freq)
            c = 1480;
            lambda = c/src_freq;
            baseline = lambda/2;
            
            pos = [];
            pos.x = 0:baseline:(baseline*(num_sensors-1));
            pos.x = pos.x - max(pos.x)/2;
            
            pos.y = zeros(1, numel(pos.x));
            pos.z = zeros(1, numel(pos.x));
            
        end
        
        function [index] = GetTwoSplitSubarrayIndex(obj)
            
            for idx = 1:obj.num_sensors
                sub_array = idx;
                sub_aperture = [obj.sensor_pos.x(1:sub_array); obj.sensor_pos.x(obj.num_sensors-sub_array+1:end)];
                D = sub_aperture(1,end) - sub_aperture(1,1);
                aperture_spacing = sub_aperture(2,1) - sub_aperture(1,1);
                if aperture_spacing <= D/2.5
                    break;
                end
            end
            
            index = zeros(2, sub_array);
            index(1,:) = 1:sub_array;
            index(2,:) = obj.num_sensors-sub_array+1:obj.num_sensors;
            
        end
        
        function [index] = GetMultipleSubarrayIndex(obj)
            
            index = [];
            
            subarray_per_elems = 32;
            num_overlaped = 16;
            %num_overlaped = 4;
            %num_overlaped = subarray_per_elems/4;

            for idx = 1:15
            %for idx = 1:9
                start_idx = (idx-1)*(subarray_per_elems - num_overlaped) + 1;
                index = [index; start_idx:(start_idx+subarray_per_elems-1)];

            end
            
        end
        
        function [ index ] = GetSubarrayElementIndex(obj, method)
            if strcmpi(method, 'conventional')
                [ index ] = GetTwoSplitSubarrayIndex(obj);
                
            elseif strcmpi(method, 'multiple')
                [ index ] = GetMultipleSubarrayIndex(obj);
                
            else
                assert(0~=0, 'method type error')
            end
            
        end
        
        function [cbf] = Beamforming(obj)
            
            beam_count = length(obj.sim_info.bot_angle);
            data_count = size(obj.IQ, 1);
            cbf = zeros(data_count, beam_count, obj.num_sensors);
            obj.simulation = 0;
            if obj.simulation                

                
                replica = SteeringVector(obj, obj.sensor_pos, obj.src_freq, obj.sim_info.bot_angle);

                for ang = 1:beam_count
                    for channel = 1:obj.num_sensors
                        cbf(:,ang,channel) = obj.IQ(:,channel) .* replica(ang, channel);
                    end    

                    disp_msg = ['Make reference Delay & SUM.. ' , num2str((ang/beam_count)*100) , '%'];
                    disp(disp_msg)
                end
                
            else
                
                SP_final = obj.sim_info.SP_final;
                HS_final = obj.sim_info.HS_final;
                xcorr_data = obj.IQ;
                
                [weight, ~] = window_make(obj.num_sensors, 4); % hamming Window 적용
                weight = weight / max(weight);
                
                data_count = data_count - max(max(SP_final));
                
                for k = 1:length(obj.sim_info.bot_angle)
                    for i =1:obj.num_sensors % 채널0
                        move_data=xcorr_data(SP_final(i,k):SP_final(i,k)+data_count,i); % +1하는이유는 기준이 지금 0이므로 행렬이 안잡힘..1로변경
                        cor_data2=move_data;
                        corr_ref2 = fftshift(fft(cor_data2));
                        corr_ref2 = ifftshift(corr_ref2);
                        corr_ref2 = abs(corr_ref2).*exp(1i*angle(corr_ref2)+1i*(HS_final(i,k))); % -pi/4 shift
                        cbf(:,k,i) = ifft(corr_ref2).*weight(i);
                    end
                end
            end
            
        end
        
        function [bf_partial] = GetBeamformResult(obj, cbf, split_idx)
            
            num_subarray = size(split_idx, 1);
            beam_count = length(obj.sim_info.bot_angle);
            data_count = size(obj.IQ, 1);
            bf_partial = zeros(data_count, beam_count, num_subarray);
            
            for idx = 1:num_subarray
                extracted = cbf(:,:,split_idx(idx,:));
                bf_partial(:,:,idx) = mean(extracted, 3);
                
            end
            
        end
        
        function [cost] = LeastSquare(obj, split_idx, bf, subarray_idx, bf_full)
            bf = permute(bf, [1 3 2]);
            data_count = size(bf, 1);
            M = size(bf, 2);
            beam_count = size(bf, 3);
            
            
            x_pos = obj.sensor_pos.x;
            start_idx = split_idx(:,1);
            
            num_sensors_subarray = obj.num_sensors - start_idx(end) + 1;
            
            xi = zeros(M, 1);
            %for idx = 1:M-1
            %for idx = 1:9-1
            for idx = 1:M
            %for idx = 1:8
                % center pos
                %xi(idx) = sum(x_pos(1+(idx-1)*num_sensors_subarray:idx*num_sensors_subarray))/num_sensors_subarray;
                xi(idx) = mean(x_pos(subarray_idx(idx,:)));
                
            end
            
            %beam_count = 1; %임시 테스트용
            
            cost = zeros(data_count, beam_count);
            min_cost = zeros(beam_count, 1);
            min_idx = zeros(beam_count, 1);
            
            for jdx = 1:beam_count
                for idx = 1:data_count
                    theta_i = (unwrap(angle(bf(idx,:,jdx))));
                    %plot(unwrap(theta_i));
                    [ cost(idx, jdx) ] = EstimateLSParams(obj, M, xi, theta_i);
                    %pause(1);
                    %drawnow;
                end
                %[min_cost(jdx), min_idx(jdx)] = min(cost(200:end,jdx));
            end
            
            
%             resolution_pad = 10;
%             detected = zeros(data_count+2*resolution_pad, beam_count);
%             
%             for idx = 1:beam_count
%                 %detected(min_idx(idx), idx) = 1;                
%                 detected(min_idx(idx):min_idx(idx)+2*resolution_pad, idx) = 1;
%                 
%             end
%             detected = detected(1:data_count, 1:beam_count);
%             %figure; imagesc(obj.sim_info.bot_angle, 1:data_count, detected*3 + cost./max(max(cost)))
%             figure; imagesc(detected*3 + cost./max(max(cost)))
            
        end
        
        function [ cost ] = EstimateLSParams(obj, M, xi, theta_i)
            
            if size(theta_i, 1) == 1
                theta_i = theta_i.';
            end
            
            if size(xi, 1) == 1
                xi = xi.';
            end
            
            %theta_i = unwrap(theta_i);
            
            a = M.*(sum(xi.^2)) - sum(xi).^2;
            
            b_est = ( 1/a ) .* ( sum(xi.^2) .* sum(theta_i) ) - (sum(xi) .* sum(xi.*theta_i));
            
            k_est = ( 1/a ) .* ( M .* sum(xi.*theta_i) - sum(xi).*sum(theta_i) );
            
            cost = sum((k_est.*xi+b_est-theta_i).^2);
        end
        
        
        function [data, data_info, FLS_colormap] = GetExperimentsData(obj)
            %C:\개발2팀 최종권\WorkSpace_Git\MATLAB\Subarray_Watertank
            %[file, path] = uigetfile('../../Subarray_Watertank/MBES 데이터/20191030/*.TEXT','Select a File');
            [file, path] = uigetfile('C:/Users/user/Documents/MATLAB/data/mbes/*.TEXT','Select a File');
            

            if isequal(file, 0)
                assert(0~=0, "파일선택안함");
            end

            temp = split(path(1:end-1), '\');
            
            for idx = 1:length(temp)
                if strcmpi(temp{idx}, 'cw')
                    waveform_type = 'cw';
                elseif strcmpi(temp{idx}, 'cp')
                    waveform_type = 'lfm';
                else
                    waveform_type = 'cw';
                end
                
            end
            
            sel_pwd = str2double(temp{end});


            temp = split(file(1:end-5), ' ');

            valid_idx = [];
            for idx = 1:length(temp)
                if ~isempty(temp{idx})
                    valid_idx = [valid_idx; idx];
                end
            end
            temp = temp(valid_idx);
            for idx = 1:length(temp)
                splited_temp = split(temp{idx}, '-');
                token = splited_temp{1};
                val = str2double(splited_temp{2});
                if strcmp('Range', token)
                    range = val; %meter

                %elseif strcmp('PWD', token)

                elseif strcmp('TVG', token)
                    tvg = val;

                elseif strcmp('Gain', token)
                    gain = val;

                elseif strcmp('Data', token)
                    data_count = val;

                end

            end

            data_info = [];
            

            csv_list = dir(fullfile(path, '*.csv'));
            for idx = 1:length(csv_list)
                sel_csv = csv_list(idx);
                temp = split(sel_csv.name, '_');
                data_path = fullfile(sel_csv.folder, sel_csv.name);
                if strcmpi(temp{2}(1:3), 'Mag')
                    mag = load(data_path);

                else
                    phase = load(data_path);
                end

            end
            data = [];
            data.real = (mag - 8192) ./ 8192;
            data.imag = (phase - 8192) ./ 8192;

            load('FLS_colormap');
            FLS_colormap = FLS_colormap / 255;
            
            data_info.pulse_type = waveform_type;
            data_info.range = range;
            data_info.pwd = sel_pwd;
            data_info.tvg = tvg;
            data_info.gain = gain;
            data_info.data_count = data_count;
            
        end
        
    end
end

