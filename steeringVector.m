function [e,test] = steeringVector(xPos, yPos, zPos, f, c, theta_angles)
% ����ǥ�� ���
% theta = elevation
% phi = azimuth

% angle -> radians
phi_angles = 0;
theta_angles = theta_angles*pi/180;
phi_angles = phi_angles*pi/180;
 
% wavenumber
k = 2*pi*f/c;

% sensor ��
P = numel(xPos);

% ����ǥ�� wavenumber vector
N = numel(phi_angles);

x = sin(theta_angles)'*cos(phi_angles);
y = sin(theta_angles)'*sin(phi_angles);
z = repmat(cos(theta_angles)', 1, N);

% steering vector
xx = bsxfun(@times, x, reshape(xPos, 1, 1, P));
yy = bsxfun(@times, y, reshape(yPos, 1, 1, P));
zz = bsxfun(@times, z, reshape(zPos, 1, 1, P));

e = exp(1j*k*(xx + yy + zz));
e = squeeze(e);
test = k*(xx + yy + zz);
