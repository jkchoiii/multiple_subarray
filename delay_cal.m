function [startpoint, Hsrad] = delay_cal(A,scan_angle,lambda_t,lambda_s,ch,beam_ch,c,ps)

% A = 센서 간격
% scan_angle = Steering Beam Angle
% lambda_t = Target 주파수의 파장 ex 200e3
% lambda_s =sampling 주파수의 파장 ex 500e3/14
% ch = 채널 갯수 ex 32
% beam_ch = steering 갯수 ex 128
An = 0 : A : A*(ch-1);
An = An-max(An)/2; % 중앙에서 부터 센서 까지의 거리
 
Hn = An'.*sind(scan_angle); % 채널별 지연 거리
% Hn = sind(scan_angle); % 채널별 지연 거리
Hn = Hn';
Hdeg = 360*(Hn / lambda_t);% 지연 거리 -> 위상으로 변환
Hrad = deg2rad(Hdeg);
if ps == 1
    Hsrad = rem(Hrad,2*pi);
    startpoint= ones(ch,beam_ch);
else
   
Sn=fix(Hn/lambda_s); % 채널별 지연 거리 -> 샘플 단위로 변환 (참조 샘플링 데이터)
total_move_min = min(min(Sn));
startpoint = Sn-total_move_min+1; %% 모든 각도의 센터의 시작점은 동일, 거기서 +- 몇칸인지 정해야됨
startpoint = startpoint';
Hsn = Hn - (Sn*lambda_s); % 참조 샘플링 데이터 기준으로 지연 거리차이( 위상 보상을 위해 )
 
Hsdeg = 360*(Hsn / lambda_t); % 샘플링 데이터 기준으로 지연 거리차이
Hsrad = deg2rad(Hsdeg);
Hsrad = rem(Hsrad,2*pi);
 
end
 
 
for i =1:beam_ch
    for k = 1:ch
        if Hsrad(i,k)<=pi && Hsrad(i,k)>=-pi
            Hsrad(i,k) = Hsrad(i,k);
        elseif Hsrad(i,k)< -pi
            Hsrad(i,k) = Hsrad(i,k) + 2*pi;
        else
            Hsrad(i,k) = Hsrad(i,k)- 2*pi;
        end
    end
end
%
% aa=c/lambda_t;
% time = Hsdeg /(360*aa);
% dis = time * c;
%
% fn = Sn*lambda_s +dis;
% err = Hn-fn;
% if sum(sum(err)) >= 1
%     disp('위상 보정 에러 발생 ')
% end
 
Hsrad = Hsrad';