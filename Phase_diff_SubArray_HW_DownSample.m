% subarray 방식�? point 방식 비교


clear all
close all

c = 1480;
fs = 80e6/28/28;
ch = 256;
% beam_ch = 90;
% beam_steering = 90;
% scan_angle = linspace(-beam_steering/2,beam_steering/2,beam_ch); % steering beam angle


freq = 400e3;
bw = 50e3;
lambda = c/(455e3);
d = lambda/2;

xPos = 0 : d : d*(ch-1);
xPos = xPos-max(xPos)/2;
yPos = zeros(1, numel(xPos)); % y�? 1xP 벡터 unit=[m]
zPos = zeros(1, numel(xPos)); % z�? 1xP 벡터 unit=[m]

%pwd = 3000e-6;
pwd = 350e-6;

t = 0:1/fs:pwd;

target_x = linspace(-50,50,500);
target_y = ones(1,length(target_x))*5;


for i = 1:length(target_x)
    Target_dis(i) = norm([target_x(i) target_y(i)]-[0 0]); %
    Target_ang(i) = atan2(target_x(i),target_y(i))*180/pi;
end

% scan_angle = [Target_ang(1) Target_ang(end/2) Target_ang(end)] ;
scan_angle = Target_ang;
delay_time = Target_dis/c;
delay_sample = delay_time/(1/fs);
max_sample = round(max(delay_sample)*1.5);
amplitude = 1./10.^(Target_dis/20);
amplitude = amplitude ./max(amplitude);

w = hamming(length(t));
%% lfm ?�� 경우?���? ?��?��
start_freq = -bw/2;
end_freq = bw/2;
kk = bw/pwd;    % Chirp ?��?�� ?��?�� 계수
input = (exp(1i*2*pi*(kk/2*t+start_freq).*t));
ref_sig = input .*w';
%%
 input = exp(1i*2*pi*0*t);
 ref_sig = exp(1i*2*pi*0*t).*w';
zeros_data = complex(zeros(1,max_sample));
zeros_data(1:length(ref_sig)) = ref_sig;
ref_sig = zeros_data;
clearvars zeros_data


ref_data = zeros(max_sample,ch);
S_Vector = steeringVector(xPos, yPos, zPos, freq, c, Target_ang);
ref_sig = repmat(ref_sig,ch,1)';

tic
for i = 1:length(target_x)
    a = delayseq(ref_sig,delay_time(i),fs).*S_Vector(i,:);
    ref_data = ref_data + a*amplitude(i);
    disp_msg = ['Make signal.. ' , num2str((i/length(target_x))*100) , '%'];
    disp(disp_msg)
end

toc



% clearvars ref_data
% load('ref_data');
%ref_data = awgn(ref_data, -5, 'measured');
%ref_data = awgn(ref_data, 0);
Raw_data = ref_data;
clearvars move_600k band_600k zero_base
pspectrum(Raw_data(:,1),fs)
% sub_array_coff = 30;
% sub_array_dis = round(sub_array_coff*lambda / d);


% sub_array = sub_array_dis;
% bf_res = zeros(size(Raw_data, 1), length(scan_angle), sub_array,2);
% enabled_ch = zeros(2, sub_array);
% enabled_ch(1,:) = 1:sub_array;
% enabled_ch(2,:) = sub_array+1:sub_array*2;
%

%%%%%%%%%%%%%%
% lfm?�� 경우 매칭 진행 .. 
Raw_data = filter(fliplr(input),1,Raw_data); % lfm?���? ?��?��
for idx = 1:numel(xPos)
    sub_array = idx;
    sub_aperture = [xPos(1:sub_array); xPos(numel(xPos)-sub_array+1:end)];
    D = sub_aperture(1,end) - sub_aperture(1,1);
    aperture_spacing = sub_aperture(2,1) - sub_aperture(1,1);
    if aperture_spacing <= D/2.5
        break;
    end
end

ref_spacing = sub_array;

% bf_res = zeros(size(Raw_data, 1), length(scan_angle), ref_spacing,2);
enabled_ch = zeros(2, ref_spacing);
enabled_ch(1,:) = 1:ref_spacing;
enabled_ch(2,:) = ch-ref_spacing+1:ch;
%%%%%%%%%%%%%%%%%%

clearvars bf_res out

% Phase_diff1 = angle(out(:,:,1));
% Phase_diff2 = angle(out(:,:,2));


bf_ref = zeros(size(Raw_data, 1), length(scan_angle),ch);


replica = steeringVector(xPos,yPos,zPos,freq, c, scan_angle);
for ang = 1:length(scan_angle)
    for cn = 1:ch
        data = Raw_data(:,cn);
        bf_ref(:,ang,cn) = data.*replica(ang,cn);
    end
    disp_msg = ['Make reference Delay & SUM.. ' , num2str((ang/length(scan_angle))*100) , '%'];
    disp(disp_msg)
end


subarray_per_elems = 32;
%num_overlaped = subarray_per_elems/4;
num_overlaped = 4;
cnt = 0;
array_idx = [];
for idx = 1:9
    cnt= cnt+1;
    start_idx = (idx-1)*(subarray_per_elems - num_overlaped) + 1;
    array_idx = [array_idx;start_idx:(start_idx+subarray_per_elems-1)];
    
end
enabled_ch = array_idx;

REF_DAS = fliplr(abs(mean(bf_ref,3)));
out = zeros(size(Raw_data, 1), length(scan_angle),2);
for index = 1:size(array_idx, 1)
    data = bf_ref(:,:,enabled_ch(index,:));
    out(:,:,index) = mean(data,3);
end

plot_idx = 1;

Phase_diff = angle(out(:,:,1) .* conj(out(:,:,2)));

phase_slope = (Phase_diff - zeros(size(Phase_diff))).^2;
zero_crossing_idx = (phase_slope(:,plot_idx)<0.5);


figure; plot(Phase_diff(:,plot_idx)); hold on; plot(find(zero_crossing_idx(:,1)), Phase_diff((zero_crossing_idx(:,1)), plot_idx), '*')

smooth_phase = smoothdata(Phase_diff(:,plot_idx));
phase_slope = (smooth_phase- zeros(size(smooth_phase))).^2;
zero_crossing_idx = (phase_slope<0.5);
figure; plot(smooth_phase); hold on; plot(find(zero_crossing_idx(:,1)), smooth_phase((zero_crossing_idx(:,1)), 1), '*')

figure;
rr = linspace(1,(size(REF_DAS,1)*1/(fs))*(c),size(REF_DAS,1));
imagesc(REF_DAS);
%imagesc(scan_angle,rr,REF_DAS);
%load('NEW_COLOR.mat')
%colormap(NEW_Color/255)

rr = linspace(0,(size(REF_DAS,1)*1/fs)*(c),size(REF_DAS,1));

%% Create grids and convert polar coordinates to rectangular
figure;polarPcolor(rr,scan_angle,REF_DAS);


% [theta,r] = meshgrid(scan_angle,rr);
% [xx,yy] = pol2cart(theta,r);
% % plot_data_corr_DSUM = imadjust(AA);
% II = (REF_DAS);
% figure;
% surf(xx,yy,II,'edgecolor','interp');
% colormap(NEW_Color./255)
% view(0,90)
% caxis([ 0 0.005] )

figure;
for idx = 1:10:size(Phase_diff, 2)
    hold off;
    Phase_diff(:,idx) = (Phase_diff(:,idx));
    plot(Phase_diff(:,idx) ./ pi);

    hold on
    REF_DAS(:,idx) = REF_DAS(:,idx);
    [~, max_idx] = max(REF_DAS(:,idx));
    plot(REF_DAS(:,idx)./max(REF_DAS(:,idx)));

    hold on; stem(max_idx, 1, 'r--');
    title(['Beamsteering Angle = ', num2str(scan_angle(idx)) '  subarray distance = ', num2str(sub_array_coff),'{\lambda}'])
    hold on; plot(zeros(size(Phase_diff,1)), 'r--');
    pause(1)
end

%  findsignal(Phase_diff(:,beam_no),ref_sig)